package by.training.utility;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyHandler implements InvocationHandler {
	private Object obj;

	public MyHandler(final Object obj) {
		this.obj = obj;
	}

	@Override
	public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
		System.out.println("Proxy MyHandler invoke " + method.getName() + "() :");
		return method.invoke(obj, args);
	}
}
