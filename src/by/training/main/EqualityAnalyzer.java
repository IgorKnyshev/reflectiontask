package by.training.main;

import by.training.annotations.Equal;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class EqualityAnalyzer {
	private List<String> aList = new ArrayList<>();
	private List<String> bList = new ArrayList<>();
	private List<String> annotationValue = new ArrayList<>();

	public boolean equalObjects(final Object a, final Object b) {
		try {
			for (Field field : a.getClass().getDeclaredFields()) {
				if (field.isAnnotationPresent(Equal.class)) {
					field.setAccessible(true);
					aList.add(field.get(a).toString());
					annotationValue.add(field.getAnnotation(Equal.class).compareBy().
							toString().toLowerCase());
				}
			}
			for (Field field : b.getClass().getDeclaredFields()) {
				if (field.isAnnotationPresent(Equal.class)) {
					field.setAccessible(true);
					bList.add(field.get(b).toString());
				}
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		boolean state = true;
		if (aList.size() != bList.size()) {
			state = false;
		} else {
			for (int i = 0; i < aList.size(); i++) {
				if ("reference".equals(annotationValue.get(i))) {
					if (aList.get(i) != bList.get(i)) {
						state = false;
						break;
					}
				}
				if ("value".equals(annotationValue.get(i))) {
					if (!aList.get(i).toLowerCase().equals(bList.get(i).toLowerCase())) {
						state = false;
						break;
					}
				}
			}
		}
		return state;
	}

}
