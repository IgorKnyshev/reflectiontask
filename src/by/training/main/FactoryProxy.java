package by.training.main;

import by.training.annotations.Proxy;
import by.training.interfaces.IMove;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;

public class FactoryProxy {
	public Object getInstanceOf(final Class<?> name) {
		Object returnObject = null;
		Annotation annotation = name.getAnnotation(Proxy.class);
		try {
			if (annotation != null) {
				Proxy proxy = (Proxy) annotation;
				Constructor beanConstructor = name.getConstructor();
				Object beanObject = beanConstructor.newInstance();

				Class<?> handlerClass = Class.forName(proxy.handlerName());
				Constructor handlerConstructor = handlerClass.getConstructor(Object.class);
				Object handlerObject = handlerConstructor.newInstance(beanObject);

				IMove proxyExample = (IMove) java.lang.reflect.Proxy.newProxyInstance(
						name.getClassLoader(),
						name.getInterfaces(),
						(InvocationHandler) handlerObject);
				System.out.println(proxyExample.move());
				returnObject = proxyExample;

			} else {
				Constructor constructor = name.getConstructor();
				returnObject = constructor.newInstance();
				IMove moveObj = (IMove) returnObject;
				System.out.println(moveObj.move());
			}
		} catch (ClassNotFoundException | InstantiationException
				  | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}

		return returnObject;
	}
}
