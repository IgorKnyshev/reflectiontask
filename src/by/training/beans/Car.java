package by.training.beans;

import by.training.annotations.Equal;
import by.training.annotations.Proxy;
import by.training.interfaces.IMove;
import by.training.utility.Type;

@Proxy(handlerName = "by.training.utility.MyHandler")
public class Car implements IMove {
	@Equal(compareBy = Type.VALUE)
	private String model;
	@Equal(compareBy = Type.VALUE)
	private int velocity;

	public Car() {
		model = "Mazda";
		velocity = 99;
	}

	public Car(final String model, final int velocity) {
		this.model = model;
		this.velocity = velocity;
	}

	public String getModel() {
		return model;
	}

	public void setModel(final String model) {
		this.model = model;
	}

	public int getVelocity() {
		return velocity;
	}

	public void setVelocity(final int velocity) {
		this.velocity = velocity;
	}

	public String move() {
		return model + " current speed = " + velocity + " km/h";
	}

}
