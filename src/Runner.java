import by.training.beans.Car;
import by.training.main.EqualityAnalyzer;
import by.training.main.FactoryProxy;

public class Runner {
	public static void main(final String[] args) {
		//A task
		System.out.println("A TASK:");
		FactoryProxy factory = new FactoryProxy();
		Object obj = factory.getInstanceOf(Car.class);

		//B task
		System.out.println("B TASK:");
		EqualityAnalyzer analyzer = new EqualityAnalyzer();
		System.out.println(analyzer.equalObjects(new Car(), new Car("mazda", 99)));
	}
}
